Build latest GCC
----------------

    docker run --rm -it -v ${PWD}:/mnt -e "GCC_VERSION=12.2.0" rockylinux:9-minimal < ./build-gdc

It should produce the `dlang-gdc.tar.bz2` file which basically contains the `/dlang` directory.

GDC Build Image
---------------

First we build an image with everything built inside:

    docker build -t gdc-build -f Containerfile.build --build-arg gcc_version=10.4.0

Build
-----

Build the Docker image:

    docker build -t gdc .

This will build `gdc:latest` ... You will get something like the following
in the command line:
    
    ...
    Successfully built 3814856c618b
    Successfully tagged gdc:latest

The build process print out the result of the `gdc --version`, so that we can use it to properly
tag the image:

    # We have to use 9-minimal for gdc because only version 9 has GDC in the EPEL repository... sigh...
    FROM rockylinux:9-minimal

    Step 6/7 : RUN echo $(gdc --version)
    ---> Running in 4c7cb925d7bc
    gdc (GCC) 11.2.1 20220127 (Red Hat 11.2.1-16) Copyright (C) 2021 Free Software Foundation, Inc. This is free software; 
    see the source for copying conditions. There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Now that we know the version of GDC inside we can properly tag the image:

     docker tag 3814856c618b gdc:11.2.1-5

Number 5 here is the build-number (I just keep increasing it until) I am
happy with the resulting image.

Share
-----

Login to docker hub:

    docker login -u <username>

We need to create repository tag:

    docker tag gdc dejlek/gdc

Push both the latest and the tag that contains the build number:

    docker push dejlek/gdc:latest
    docker push dejlek/gdc:11.2.1-5

Also, tag particular version+build, and push:

    docker tag 3814856c618b dejlek/gdc:11.2.1-5
    docker push dejlek/gdc:11.2.1-5
