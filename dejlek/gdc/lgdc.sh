#!/bin/sh
# Minimal wrapper around ldc2 in order to build gdc-12+
# ../configure --enable-languages=d GDC=lgdc.sh
TRANSLATED_ARGS=
while [ $# -gt 0 ]; do
    case "$1" in
	-fPIC)
	    TRANSLATED_ARGS="${TRANSLATED_ARGS} --relocation-model=pic"
	    shift
	    ;;
	-frelease)
	    TRANSLATED_ARGS="${TRANSLATED_ARGS} --release"
	    shift
	    ;;
	-fversion=*)
	    TRANSLATED_ARGS="${TRANSLATED_ARGS} --d-version=`echo $1 | sed 's/.*=//g'`"
	    shift
	    ;;
	-o)
	    shift
	    TRANSLATED_ARGS="${TRANSLATED_ARGS} -of=${1}"
	    shift
	    ;;
        -static-libphobos)
            TRANSLATED_ARGS="${TRANSLATED_ARGS} -link-defaultlib-shared=false"
            shift
            ;;
        -static-libstdc++)
            TRANSLATED_ARGS="${TRANSLATED_ARGS} --static"
            shift
            ;;
	-Wall)
	    TRANSLATED_ARGS="${TRANSLATED_ARGS} -wi"
	    shift
	    ;;
	-Wdeprecated)
	    TRANSLATED_ARGS="${TRANSLATED_ARGS} --dw"
	    shift
	    ;;
	-no-pie|-l*|-L*)
	    TRANSLATED_ARGS="${TRANSLATED_ARGS} -L$1"
	    shift
	    ;;
	-MF)
	    shift
	    TRANSLATED_ARGS="${TRANSLATED_ARGS} --makedeps=$1"
	    shift
	    ;;
	-MT)
	    shift
	    shift
	    ;;
	-D*|-f*|-M*|-W*|-pedantic|-rdynamic|-static-*)
	    shift
	    ;;
	*)
	    TRANSLATED_ARGS="${TRANSLATED_ARGS} $1"
	    shift
	    ;;
    esac
done
ldc2 $TRANSLATED_ARGS