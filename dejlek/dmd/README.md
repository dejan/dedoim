
Build
-----

Build the Docker image:

    docker build -t dmd .

This will build `dmd:latest` ... You will get something like the following
in the command line:

    ...
    Successfully built 3814856c618b
    Successfully tagged dmd:latest

Since we have build docker image with the dmd-2.100.0-0.fedora.x86_64.rpm package we will tag the docker image with

     docker tag 3814856c618b dmd:2.100.0-5

Number 5 here is the build-number (I just keep increasing it until) I am
happy with the resulting image.

Share
-----

Login to docker hub:

    docker login -u <username>

We need to create repository tag:

    docker tag dmd dejlek/dmd

Push the latest

    docker push dejlek/dmd:latest

Also, tag particular version+build, and push:

    docker tag 3814856c618b dejlek/dmd:2.100.0-5
    docker push dejlek/dmd:2.100.0-5
