DEDOIM - DEjan's DOcker IMages
==============================

dejlek/dmd
----------

A tiny Docker image based on RockyLinux (9-minimal) with the DMD compiler installed. DUB comes
installed with DMD.

dejlek/gdc
----------

A tiny Docker image based on RockyLinux (9-minimal) with the GDC compiler installed, as well
as the DUB package manager.
